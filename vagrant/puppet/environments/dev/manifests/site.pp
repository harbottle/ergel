# All repos before packages
Yumrepo<| |> -> Package<| |>

# EPEL
include epel

# Swap file
Service {
  require => Class['swap::config']
}
class { 'swap::config': }

# Firewall
Firewall {
  before  => Class['my_fw::post'],
  require => Class['my_fw::pre'],
}
class { ['my_fw::pre', 'my_fw::post']: }
class { 'firewall': }

# Apache
class { 'apache': }
firewall { '80 allow http access':
    dport  => 80,
    proto  => tcp,
    action => accept,
}
firewall { '443 allow https access':
    dport  => 443,
    proto  => tcp,
    action => accept,
}
apache::vhost { 'http.epmel.box':
  servername => 'epmel.box',
  port    => '80',
  docroot => '/vagrant/public',
  ssl     => false,
}
apache::vhost { 'https.epmel.box':
  servername => 'epmel.box',
  port    => '443',
  docroot => '/vagrant/public',
  ssl     => true,
}

# Other packages
package { [ 'rubygems', 'ruby-devel', 'rpm-build', 'gcc', 'gcc-c++', 'make',
            'libpcap-devel', 'wget', 'createrepo', 'git', 'npm', 'rpm-sign',
            'rubygem-bundler']:
        }
