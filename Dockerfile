FROM centos:7
LABEL maintainer="harbottle <harbottle@room3d3.com>"
# Update and repos
RUN yum -y update \
  && yum -y install epel-release \
  && yum -y install http://harbottle.gitlab.io/harbottle-main-release/harbottle-main-release-7.rpm \
  && yum clean all \
  && rm -rf /var/cache/yum
# Compiler tools
RUN yum -y install gcc gcc-c++ make \
  && yum clean all \
  && rm -rf /var/cache/yum
# RPM tools
RUN yum -y install rpm-build rpm-sign createrepo \
  && yum clean all \
  && rm -rf /var/cache/yum
# Utils
RUN yum -y install git nodejs-bower wget \
  && yum clean all \
  && rm -rf /var/cache/yum
# Libraries
RUN yum -y install glib2-devel openal-devel freeglut-devel freeimage-devel libpcap-devel libsndfile-devel libvorbis-devel libX11-devel libXinerama-devel libXmu-devel lxc-devel mesa-libGL-devel pango-devel SDL2_ttf-devel zlib-devel \
  && yum clean all \
  && rm -rf /var/cache/yum
# Ruby tools
RUN yum -y install ruby-devel rubygem-bundler \
  && yum clean all \
  && rm -rf /var/cache/yum
# Python libs
RUN yum -y install PyYAML python-colorama \
  && yum clean all \
  && rm -rf /var/cache/yum
